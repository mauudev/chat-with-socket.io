const path = require('path');
const express = require('express');
const app = express();

// settings
app.set('port', process.env.PORT || 3000);

// static files
app.use(express.static(path.join(__dirname, 'public')));
 
// start the server
const server = app.listen(app.get('port'), () => {//iniciamos un server para darle a socket.io
    console.log('server on port', app.get('port'));
});

// settings
app.set('port', process.env.PORT || 3000);

// static files
app.use(express.static(path.join(__dirname, 'public')));
 
// import socket.io
const socketIO = require('socket.io');

// socketIO.listen(server);//aca le pasamos el server creado
const io = socketIO(server);//aca le pasamos el server creado y podemos guardar en una const que es la comunicacion con websockets
// -> Hasta aqui ya esta configurado el socket.io ahora podemos iniciar las comunicaciones

// websockets
io.on('connection', (socket) => { //aca enviamos el socket del cliente al server
    console.log('New connection !', socket.id);
    
    socket.on('chat:message', (data) => {//recibimos desde el cliente el data con la firma chat:message
        io.sockets.emit('chat:message', data);// reenviamos los datos a todos los conectados
    });

    socket.on('chat:typing', (data) => {
        socket.broadcast.emit('chat:typing', data);//envio el typing.. a todos excepto al que envio (brodcast)
    });
});

