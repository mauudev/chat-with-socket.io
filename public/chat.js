const socket = io();//esta variable se encarga los eventos del cliente al servidor

// DOM elements
let message = document.getElementById('message');
let username = document.getElementById('username');
let btn = document.getElementById('send');
let output = document.getElementById('output');
let actions = document.getElementById('actions');

btn.addEventListener('click', function(){
    socket.emit('chat:message', {//enviamos estos datos al servidor
        message: message.value,
        username: username.value
    });
});

message.addEventListener('keypress', function(){//aca enviamos el username del que esta typeando al servidor para que lo reenvie
    socket.emit('chat:typing', username.value);
});

socket.on('chat:message', function(data){//aca escuchamos y renderizamos los mensajes enviados por el servidor
    actions.innerHTML = '';
    output.innerHTML += `<p>
    <strong>${data.username}</strong>: ${data.message}
    </p>`
});

socket.on('chat:typing', function(data) {
    actions.innerHTML = `<p><em>${data} is typing...</em></p>`;
});